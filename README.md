# Resume Online

This project is make for practice in vuejs and improve firebase skill

## Installation

you should use yarn 

```bash
yarn install
```

## Usage

```vuejs
yarn run serve 
or 
npm run serve
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)