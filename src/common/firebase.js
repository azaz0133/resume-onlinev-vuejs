import firebase from "firebase";

const config = {
  apiKey: "AIzaSyD_q661xjE06rX6tp34Mgghr5MWAXpilXQ",
  authDomain: "resumeonline-28ac2.firebaseapp.com",
  databaseURL: "https://resumeonline-28ac2.firebaseio.com",
  projectId: "resumeonline-28ac2",
  storageBucket: "resumeonline-28ac2.appspot.com",
  messagingSenderId: "1066537918494"
};

firebase.initializeApp(config);

const db = firebase.database();

export default {
  create: (rootName, attrs) => {
    return new Promise((resolve, reject) => {
      db.ref()
        .child(`${rootName}/`)
        .push({
          ...attrs
        })
        .then(() =>
          resolve({
            status: "ok",
            statusCode: 201,
            message: "created"
          })
        )
        .catch(err =>
          reject({
            status: "err",
            statusCode: 404,
            message: err
          })
        );
    });
  },

  update: (rootName, key, attrs) => {
    return new Promise((resolve, reject) => {
      db.ref(`${rootName}/`)
        .child(key)
        .update({
          ...attrs
        })
        .then(() =>
          resolve({
            status: "ok",
            statusCode: 201,
            message: "updated"
          })
        )
        .catch(err =>
          reject({
            status: "err",
            statusCode: 404,
            message: err
          })
        );
    });
  },

  destroy: (rootName, key) => {
    return new Promise((resolve, reject) => {
      db.ref(rootName)
        .child(key)
        .remove()
        .then(() =>
          resolve({
            status: "ok",
            statusCode: 201,
            message: "deleted"
          })
        )
        .catch(err =>
          reject({
            status: "err",
            statusCode: 404,
            message: err
          })
        );
    });
  },
  other: db,
  pushToArray: (seat, array) => {
    const ids = array.map(s => s);
    const idDubCheck = ids.indexOf(seat.id);
    if (idDubCheck === -1) {
      array.push(seat);
    } else {
      array.splice(idDubCheck, 1);
    }
  }
};
